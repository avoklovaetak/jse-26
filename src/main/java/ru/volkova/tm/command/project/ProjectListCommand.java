package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.enumerated.Sort;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "show project list";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final String sort = TerminalUtil.nextLine();
        List<Project> projects;
        if (sort.isEmpty()) projects = serviceLocator.getProjectService().findAll(userId);
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(userId, sortType.getComparator());
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println((index + ". " + project));
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

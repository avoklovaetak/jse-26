package ru.volkova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show developer info";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) return;
        System.out.println("[ABOUT]");
        System.out.println("NAME: " + Manifests.read("developer"));
        System.out.println("E-MAIL: " + Manifests.read("email"));
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

}

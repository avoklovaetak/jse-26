package ru.volkova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String middleName;

    @Nullable
    private String firstName;

    @Nullable
    private String secondName;

    @NotNull
    private Boolean locked = true;

    @Nullable
    private Role role = Role.USER;

}

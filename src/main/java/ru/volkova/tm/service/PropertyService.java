package ru.volkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password_secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password_iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_DEVELOPER_NAME_KEY = "application_developer";

    @NotNull
    private static final String APPLICATION_DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_DEVELOPER_EMAIL_KEY = "application_email";

    @NotNull
    private static final String APPLICATION_DEVELOPER_EMAIL_DEFAULT = "";


    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY)) return System.getProperty(PASSWORD_SECRET_KEY);
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) return System.getenv(PASSWORD_SECRET_KEY);
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getProperties().containsKey(PASSWORD_ITERATION_KEY)){
            final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)){
            final String value = System.getenv(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        final String value = properties.getProperty(PASSWORD_ITERATION_KEY,PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getProperties().containsKey(APPLICATION_VERSION_KEY))
            return System.getProperty(APPLICATION_VERSION_KEY);
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY))
            return System.getenv(APPLICATION_VERSION_KEY);
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        if (System.getProperties().containsKey(APPLICATION_DEVELOPER_NAME_KEY))
            return System.getProperty(APPLICATION_DEVELOPER_NAME_KEY);
        if (System.getenv().containsKey(APPLICATION_DEVELOPER_NAME_KEY))
            return System.getenv(APPLICATION_DEVELOPER_NAME_KEY);
        return properties.getProperty(APPLICATION_DEVELOPER_NAME_KEY, APPLICATION_DEVELOPER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        if (System.getProperties().containsKey(APPLICATION_DEVELOPER_EMAIL_KEY))
            return System.getProperty(APPLICATION_DEVELOPER_EMAIL_KEY);
        if (System.getenv().containsKey(APPLICATION_DEVELOPER_EMAIL_KEY))
            return System.getenv(APPLICATION_DEVELOPER_EMAIL_KEY);
        return properties.getProperty(APPLICATION_DEVELOPER_EMAIL_KEY, APPLICATION_DEVELOPER_EMAIL_DEFAULT);
    }

    public static void main(String[] args) {
        final PropertyService propertyService = new PropertyService();
        System.out.println(propertyService.getPasswordSecret());
    }

}

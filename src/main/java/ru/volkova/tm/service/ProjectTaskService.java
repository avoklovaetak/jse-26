package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.IProjectTaskService;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        taskRepository.bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @Override
    public List<Task> findAllTasksByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeProjectById(
            @NotNull final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        taskRepository.unbindTaskByProjectId(userId, projectId, taskId);
    }

}
